import { graphql } from 'gatsby';

export const productField = graphql`
  fragment ShopifyProductFields on ShopifyProduct {
    shopifyId
    availableForSale
    title
    description
    images {
      id
      localFile {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  }
`;
