import React from 'react';
import { FooterWrapper, Left, Right, Social, Container } from './styles';
import { FaFacebook, FaInstagram } from 'react-icons/fa';

export function Footer() {
  return (
    <FooterWrapper>
      <Container>
        <Left>
          <h2>Kontakti</h2>
          <p>SIA Mazliet vairāk gaisa</p>
          <p>20266572</p>
          <p>martina.ballite@gmail.com</p>
          <p>Reģ. Nr. - 53603090431</p>
          <p>Jelgava, Pērnavas iela 12 - 46, LV-3004</p>
          <Social>
            <p>Seko mums</p>
            <div>
              <a href="https://www.facebook.com/martinaballite/">
                <FaFacebook size="1.5em" />
              </a>
              <a href="https://www.instagram.com/martinaballite/">
                <FaInstagram size="1.5em" />
              </a>
            </div>
          </Social>
        </Left>
        <Right>
          {/* <p>
            Created by{' '}
            <a href="https://www.arbor-dev.com">
              <strong>Arbor Dev</strong>
            </a>
          </p> */}
          <p>bandenieks.juris@gmail.com</p>
          <p>Juris Bandenieks &copy; 2021</p>
        </Right>
      </Container>
    </FooterWrapper>
  );
}
