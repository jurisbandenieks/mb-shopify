import styled from 'styled-components';

export const FooterWrapper = styled.div`
  background-color: #9e579d;
  color: #f4f4f4;
  margin-top: 4rem;
  clip-path: polygon(0 0, 100% 20%, 100% 100%, 0% 100%);
  width: 100%;

  @media (min-width: 768px) {
    clip-path: polygon(0 0, 100% 48%, 100% 100%, 0% 100%);
  }
`;

export const Left = styled.div`
  margin-top: 1rem;

  p {
    line-height: 26px;
  }
`;

export const Right = styled.div`
  height: 100%;
  text-align: left;

  p {
    line-height: 26px;
  }

  @media (min-width: 768px) {
    text-align: right;
  }
`;

export const Social = styled.div`
  margin-top: 1rem;
  line-height: 32px;

  @media (min-width: 768px) {
    margin-bottom: 2rem;
  }

  p {
    font-size: 18px;
    margin-bottom: 1rem;
  }

  a {
    font-size: 32px;
    cursor: pointer;
    color: white;
    transition: all 0.2s ease-in-out;

    &:not(:last-child) {
      margin-right: 1rem;
    }

    &:hover {
      color: #581b98;
    }
  }
`;

export const Container = styled.div`
  width: 95%;
  margin: 0 auto;
  padding: 4rem 0;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  align-items: flex-start;

  @media (min-width: 768px) {
    width: 80%;
    flex-direction: row;
    align-items: flex-end;
  }
`;
