import React, { useEffect } from 'react';
import Image from 'gatsby-image';
import { ImageGalleryWrapper } from './styles';
import { ImageThumbnail } from './ImageThumbnail';
import { navigate, useLocation } from '@reach/router';

export function ImageGallery({ images, selectedVariantImageId, variants }) {
  const { origin, pathname } = useLocation();

  const [activeImageThumbnail, setActiveImageThumbnail] = React.useState(
    images.find(({ id }) => id === selectedVariantImageId) || images[0]
  );

  useEffect(() => {
    setActiveImageThumbnail(
      images.find(({ id }) => id === selectedVariantImageId) || images[0]
    );
  }, [selectedVariantImageId, setActiveImageThumbnail, images]);

  const handleClick = image => {
    setActiveImageThumbnail(image);

    if (!!variants.length) {
      const variant = variants.find(v => v.image.id === image.id);

      navigate(
        `${origin}${pathname}?variant=${encodeURIComponent(variant.id)}`,
        {
          replace: true,
        }
      );
    }
  };

  return (
    <ImageGalleryWrapper>
      <div>
        <Image fluid={activeImageThumbnail.localFile.childImageSharp.fluid} />
      </div>
      <div>
        {images.map(image => {
          return (
            <ImageThumbnail
              key={image.id}
              isActive={activeImageThumbnail.id === image.id}
              image={image}
              onClick={handleClick}
            />
          );
        })}
      </div>
    </ImageGalleryWrapper>
  );
}
