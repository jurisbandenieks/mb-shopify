import React from 'react';
import ProductContext from 'context/ProductContext';
import { ProductsGrid } from '../ProductsGrid';

export function FeaturedProducts() {
  const { collections } = React.useContext(ProductContext);

  const featuredCollection = collections.find(
    collection => collection.title.toLowerCase() === 'popular'
  );

  return (
    <section>
      <h1>Populārākie produkti</h1>
      {featuredCollection && (
        <ProductsGrid products={featuredCollection.products} />
      )}
    </section>
  );
}
