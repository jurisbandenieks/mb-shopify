import styled from 'styled-components';

export const SearchForm = styled.form`
  display: flex;
  margin: 10px 0;
  width: 100%;

  @media (min-width: 768px) {
    margin: 0;
    width: 250px;
  }
`;
