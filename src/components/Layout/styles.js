import styled from 'styled-components';

export const MainWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
`;

export const LayoutWrapper = styled.div`
  margin: 0 auto;
  width: 95%;
  box-sizing: border-box;
  padding: 0 20px 40px 20px;
  flex: 1;

  @media (min-width: 768px) {
    width: 80%;
  }
`;
