import React from 'react';
import { LayoutWrapper, MainWrapper } from './styles';
import { Header } from '../Header';
import { Footer } from '../Footer';

const Layout = ({ children }) => {
  return (
    <MainWrapper>
      <Header />
      <LayoutWrapper>
        <main>{children}</main>
      </LayoutWrapper>
      <Footer />
    </MainWrapper>
  );
};

export { Layout };
