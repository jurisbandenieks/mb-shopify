import React from 'react';
import { CollectionTile } from '../CollectionTile';
import { RemainingCollections } from './styles';

export function HomepageCollectionsGrid({ collections }) {
  const saleCollection = collections.find(
    collection => collection.title.toLowerCase() === 'sale'
  );

  const remainingCollections = collections.filter(
    collection => collection.title.toLowerCase() !== 'sale'
  );

  return (
    <div>
      {saleCollection && (
        <CollectionTile
          sale
          destination={`/all-products?c=${encodeURIComponent(
            saleCollection.shopifyId
          )}`}
          title={saleCollection.title}
          backgroundImage={
            saleCollection?.image?.localFile?.childImageSharp?.fluid
          }
          description={saleCollection.description}
        />
      )}
      <RemainingCollections>
        {remainingCollections.map(collection => (
          <CollectionTile
            key={collection.shopifyId}
            destination={`/all-products?c=${encodeURIComponent(
              collection.shopifyId
            )}`}
            title={collection.title}
            backgroundImage={
              collection?.image?.localFile?.childImageSharp?.fluid
            }
            description={collection.description}
          />
        ))}
      </RemainingCollections>
    </div>
  );
}
