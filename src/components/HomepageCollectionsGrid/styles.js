import styled from 'styled-components';

export const RemainingCollections = styled.div`
  display: grid;
  grid-gap: 10px;
  margin: 20px 0 40px;

  @media (min-width: 384px) {
    grid-template-columns: 1fr;
  }

  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr;
    margin: 20px 0 60px;
  }

  @media (min-width: 1024px) {
    grid-template-columns: 1fr 1fr 1fr;
  }

  > div {
    flex-grow: 1;
    min-width: 100%;

    @media (min-width: 768px) {
      min-width: 50%;
    }

    @media (min-width: 1024px) {
      min-width: 33%;
    }
  }
`;
