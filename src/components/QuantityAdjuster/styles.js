import styled from 'styled-components';

export const QuantityAdjusterWrapper = styled.div`
  display: flex;
  > div {
    margin: auto 0;
    padding: 5px 10px;
  }
`;

export const AdjusterButton = styled.button`
  cursor: pointer;
  border: 1px solid black;
  font-weight: bold;
  background-color: transparent;
  outline: none;
  width: 28px;
  font-size: 14px;
`;
