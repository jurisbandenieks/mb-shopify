import React from 'react';
import CartContext from 'context/CartContext';
import { CartItem, CartFooter, CartHeader, Footer } from './styles';
import { QuantityAdjuster } from '../QuantityAdjuster';
import { RemoveLineItem } from '../RemoveLineItem';
import { Button } from '../Button';
import { navigate } from '@reach/router';

export function CartContent() {
  const { checkout, updateLineItem } = React.useContext(CartContext);

  const handelAdjustQuantity = ({ quantity, variantId }) => {
    updateLineItem({ quantity, variantId });
  };

  return (
    <section>
      <h1>Jūsu grozs</h1>
      {!!checkout?.lineItems && (
        <CartHeader>
          <div>Produkts</div>
          <div>Vienības cena</div>
          <div>Daudzums</div>
          <div>Summa</div>
        </CartHeader>
      )}

      {checkout?.lineItems?.map(item => (
        <CartItem key={item.variant.id}>
          <div>
            <div>{item.title}</div>
            <div>
              {item.variant.title === 'Default Title' ? '' : item.variant.title}
            </div>
          </div>
          <div>€{item.variant.price}</div>
          <div>
            {' '}
            <QuantityAdjuster item={item} onAdjust={handelAdjustQuantity} />
          </div>
          <div>€{(item.quantity * item.variant.price).toFixed(2)}</div>
          <div>
            <RemoveLineItem lineItemId={item.id} />
          </div>
        </CartItem>
      ))}

      {!!checkout?.lineItems && (
        <CartFooter>
          <div>
            <strong>Kopā:</strong>
          </div>
          <div>
            <span>€{checkout?.totalPrice}</span>
          </div>
        </CartFooter>
      )}

      {!checkout?.lineItems && <h4>Jūsu grozs ir tukš</h4>}

      <Footer>
        <div>
          <Button onClick={() => navigate(-1)}>Turpināt iepirkties</Button>
        </div>
        <div>
          {!!checkout?.webUrl && checkout.lineItems && (
            <Button
              onClick={() => {
                window.location.href = checkout.webUrl;
              }}
            >
              Pirkt
            </Button>
          )}
        </div>
      </Footer>
    </section>
  );
}
