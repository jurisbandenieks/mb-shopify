import styled from 'styled-components';

export const HeaderWrapper = styled.header`
  display: flex;
  width: 95%;
  margin: 0 auto 20px;
  padding: 10px 20px;
  box-sizing: border-box;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;

  @media (min-width: 768px) {
    flex-direction: row;
    margin: 0 auto 50px;
    width: 80%;
  }
`;

export const SearcWapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;

  @media (min-width: 768px) {
    flex-direction: row;
    align-items: center;
    width: auto;
  }
`;
