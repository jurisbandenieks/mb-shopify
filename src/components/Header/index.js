import React from 'react';
import { HeaderWrapper, SearcWapper } from './styles';
import { Cart } from '../Cart';
import { Link } from 'gatsby';
import { Logo } from '../Logo';

export function Header() {
  return (
    <HeaderWrapper>
      <div>
        <Link to="/">
          <Logo />
        </Link>
      </div>
      <SearcWapper>
        <Cart />
      </SearcWapper>
    </HeaderWrapper>
  );
}
