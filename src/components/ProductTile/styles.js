import styled from 'styled-components';
import { StyledLink } from '../StyledLink';

export const ProductTileWrapper = styled.div`
  border: 1px solid #ddd;
  display: flex;
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
  overflow: hidden;
  flex-direction: column;

  > ${StyledLink} {
    border: 1px solid #9e579d;
    text-decoration: none;
    display: block;
    padding: 10px;
    text-align: center;
    font-weight: bold;
    color: #9e579d;
  }
`;

export const Title = styled.div`
  font-weight: bold;
  font-size: 20px;
  margin: 10px;
`;

export const Description = styled.div`
  color: #999;
  text-align: left;
  padding: 0 10px 10px 10px;
`;

export const Price = styled.div`
  font-style: italic;
  padding: 10px;
  font-weight: bold;
  margin-top: auto;
`;
