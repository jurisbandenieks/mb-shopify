import styled from 'styled-components';

export const ProductQuantityWapper = styled.div`
  margin-top: 20px;
  width: 100%;

  > strong {
    display: block;
    margin-bottom: 10px;
  }

  > form {
    display: grid;
    grid-template-columns: 1fr 6fr;
  }
`;
