/* eslint-disable jsx-a11y/no-onchange */

import React, { useEffect, useContext, useState } from 'react';
import { graphql } from 'gatsby';
import {
  Layout,
  ImageGallery,
  ProductQuantityAdder,
  Button,
  SEO,
} from 'components';
import { Grid, SelectWrapper, Price, Title } from './styles';
import CartContext from 'context/CartContext';
import { navigate, useLocation } from '@reach/router';
import queryString from 'query-string';

export const query = graphql`
  query ProductQuery($shopifyId: String) {
    shopifyProduct(shopifyId: { eq: $shopifyId }) {
      ...ShopifyProductFields
    }
  }
`;

export default function ProductTemplate(props) {
  const { shopifyProduct } = props.data;
  const { getProductById } = useContext(CartContext);
  const { search, origin, pathname } = useLocation();
  const variantId = queryString.parse(search).variant;

  const [product, setProduct] = useState(null);
  const [selectedVariant, setSelectedVariant] = useState(null);

  useEffect(() => {
    getProductById(shopifyProduct.shopifyId).then(result => {
      setProduct(result);
      setSelectedVariant(
        result?.variants.find(({ id }) => id === variantId) ||
          result?.variants[0]
      );
    });
  }, [
    getProductById,
    shopifyProduct,
    setProduct,
    setSelectedVariant,
    variantId,
  ]);

  const handleVariantChange = e => {
    const newVariant = product?.variants.find(v => v.id === e.target.value);
    setSelectedVariant(newVariant);
    navigate(
      `${origin}${pathname}?variant=${encodeURIComponent(newVariant.id)}`,
      {
        replace: true,
      }
    );
  };

  return (
    <Layout>
      <SEO
        title={props.data.shopifyProduct.title}
        description={props.data.shopifyProduct.description}
      />
      <Button onClick={() => navigate(-1)}>Atpakaļ</Button>
      <Grid>
        <div>
          {product && selectedVariant && (
            <ImageGallery
              selectedVariantImageId={selectedVariant.image.id}
              images={shopifyProduct.images}
              variants={product.variants}
            />
          )}
        </div>
        <div>
          <Title>{shopifyProduct.title}</Title>
          <p>{shopifyProduct.description}</p>
          {product?.availableForSale && selectedVariant && (
            <>
              {product?.variants.length > 1 && (
                <SelectWrapper>
                  <strong>Varianti</strong>
                  <select
                    value={selectedVariant.id}
                    onChange={handleVariantChange}
                  >
                    {product?.variants.map(v => (
                      <option key={v.id} value={v.id}>
                        {v.title}
                      </option>
                    ))}
                  </select>
                </SelectWrapper>
              )}
              {selectedVariant && (
                <>
                  <Price>€{selectedVariant.price}/gab.</Price>
                  <ProductQuantityAdder
                    variantId={selectedVariant.id}
                    available={selectedVariant.available}
                  />
                </>
              )}
            </>
          )}
        </div>
      </Grid>
    </Layout>
  );
}
