import styled from 'styled-components';

export const Grid = styled.section`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
  margin-top: 20px;

  > div:first-child {
    order: 2;
  }

  > div:last-child {
    order: 1;
  }

  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr;
  }
`;

export const Title = styled.h1`
  font-size: 26px;
  margin: 0 0 20px;
  padding-bottom: 5px;
  border-bottom: 1px solid #ccc;

  @media (min-width: 768px) {
    font-size: 38px;
    padding-bottom: 10px;
    margin: 0 0 30px;
  }
`;

export const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  margin: 30px 0 0;

  > strong {
    display: block;
    margin: 0 10px 0 0;
  }
`;

export const Price = styled.div`
  margin: 20px 0;
  font-weight: bold;
  font-size: 30px;
`;
